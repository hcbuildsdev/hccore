package com.asgcore.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.asgcore.Main.instance;

/**
 * Created by Gladithon on 17/10/2017.
 */
public class help implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if(cmd.getName().equalsIgnoreCase("help")) {
            Player p = (Player) sender;
            if (!sender.hasPermission("cylo.help")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', instance.getConfig().getString("messages.nopermission")));
                return true;
            }
            for (String helpcmd : instance.getConfig().getStringList("help")) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', helpcmd));
            }
        }
        return false;
    }
}
