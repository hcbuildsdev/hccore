package com.asgcore.Commands;

import com.asgcore.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.asgcore.Main.instance;

/**
 * Created by Gladithon on 17/10/2017.
 */
public class coords implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        Player p = (Player) sender;
        if(cmd.getName().equalsIgnoreCase("coords")) {
            if (!sender.hasPermission("cylo.coords")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', instance.getConfig().getString("messages.nopermission")));
                return true;
            }
            for (String coordscmd : instance.getConfig().getStringList("coords")) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', coordscmd));
            }
        }
        return false;
    }
}
