package com.asgcore;

import com.asgcore.Commands.coords;
import com.asgcore.Commands.help;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Gladithon on 17/10/2017.
 */
public class Main extends JavaPlugin{

    public static Main instance;

    public void onEnable() {
        instance.saveDefaultConfig();

        registerCommand("coords", new coords());

        registerCommand("help", new help());

        instance = this;

    }

    public void onDisable() {
        instance = null;
    }

    public void registerListener(Listener... listener) {
        for (Listener l : listener) {
            this.getServer().getPluginManager().registerEvents(l, this);
        }
    }

    public void registerCommand(String command, CommandExecutor commandExecutor){
        this.getCommand(command).setExecutor(commandExecutor);
    }

    public static Main get() {
        return instance;
    }
}
